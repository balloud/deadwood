import java.util.*;
import java.io.*;
import java.lang.*;
public class Player {

	private String playerName;
	private int rank;
	private int credits;
	private int dollars;
	private Room location;
	private String status; /* ie "moving" "acting" . . . */
	private Role currentRole;
	private Scene currentScene;
	private int rehersalChips;

	public Player(String n) {
		this.playerName = n;
		this.rank = 1;
		this.credits = 0;
		this.dollars = 0;
		this.rehersalChips = 0;

	}

	public String getPlayerName() {
		return this.playerName;
	}
	public int getRank() {
	 	return this.rank;
	}

	public int getCredits() {
		return this.credits;
	}

	public int getDollars() {
		return this.dollars;
	}

	public Room getLocation() {
		return this.location;
	}

	public void changeLocation(Room r) {
		this.location = r;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String s) {
		this.status = s;
	}

	public Role getCurrentRole() {
		return this.currentRole;
	}

	public void setRole(Role r) {
		this.currentRole = r;
	}

	public Scene getCurrentScene() {
		return this.currentScene;
	}

	public int getRehersalChips() {
		return this.rehersalChips;
	}

	public void reherse() {
		this.rehersalChips++;
	}

	public void clearRehersalChips(){
		this.rehersalChips = 0;
	}

	public void changeRank(int r) {
		this.rank = r;
	}
	public void subtractDollars(int d) {
		this.dollars = this.dollars - d;
	}

	public void subtractCredits(int c) {
		this.credits = this.credits - c;
	}

	public void addCredits(int c) {
		this.credits += c;
	}

	public void addDollars(int d) {
		this.dollars += d;
	}

}