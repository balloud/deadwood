import java.util.*;
import java.io.*;
import java.lang.*;

public class Room {

	private List<String> roomList = new ArrayList<String>();	/* Adj list */
	private List<Player> playerList = new ArrayList<Player>();		
	private String name;
	private int shotCounter;
	private int backupShotCounter;
	private List<Role> roles = new ArrayList<Role>();
	private Scene scene;
	private boolean hasScene;

	public Room(String n, List rooms) {
		this.name = n;
		this.roomList = rooms;
	}	

	public void addRemovePlayer(Player p) {
		if (this.playerList.contains(p)) {
			this.playerList.remove(p);
		} else {
			this.playerList.add(p);
		}
	}

	public Scene getScene() {
		return this.scene;
	}	

	public boolean doesHaveScene() {
		return this.hasScene;
	}

	public void addScene(Scene s) {
		this.scene = s;
		this.hasScene = true;
	}

	public void removeScene() {
		this.scene= null;
		this.hasScene = false;
	}

	public void addRoom(String r) {
		this.roomList.add(r);
	}

	public List<String> getRoomList() {
		return this.roomList;
	}

	public List<Player> getPlayerList() {
		return this.playerList;
	}

	public String getName() {
		return this.name;
	}

	public void removeShot() {
		this.shotCounter--;
	}

	public void setShotCounter(int s) {
		this.shotCounter = s;
	}

	public int getShotCounter() {
		return this.shotCounter;
	}

	public void shotComplete() {
		this.shotCounter --;
	}

	public void addRole(Role r) {
		this.roles.add(r);
	}

	public List<Role> getRoles() {
		return this.roles;
	}
	public void setBackupShotCounter(int b) {
		this.backupShotCounter = b;
	}
	public void restoreShotCounter() {
		this.shotCounter = this.backupShotCounter;
	}
	public void removeRole(Role r) {
		this.roles.remove(r);
	}


	




}