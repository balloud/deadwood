import java.util.*;
import java.io.*;
import java.lang.*;
public class SoundStage extends Room{

	Scene scene;
	
	boolean hasScene;

	public SoundStage(String n, List roomList) {
		super(n, roomList);		
		this.hasScene = true;		
	}

	public void removeScene() {
		if (this.hasScene == true) {
			this.hasScene = false;
		}
	}

	public void addScene(Scene s) {
		this.scene = s;
	}

}