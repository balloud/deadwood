import java.util.*;
import java.io.*;
import java.lang.*;
public class Scene {

	List<Role> roleList = new ArrayList<Role>();	
	List<Player> playerList = new ArrayList<Player>();	
	String name;
	String description;
	int number;
	int budget;

	public Scene(String n, String d, int num, int b) {
		this.name = n;
		this.description = d;
		this.number = num;
		this.budget = b;
	}

	public void addRemovePlayer(Player p) {
		if (this.playerList.contains(p)) {
			this.playerList.remove(p);
		} else {
			this.playerList.add(p);
		}
	}

	public void removeRole(Role r) {
		this.roleList.remove(r);
	}

	public void addRole(Role r) {
		this.roleList.add(r);
	}

	public List<Role> getRoleList() {
		return this.roleList;
	}

	public List getPlayerList() {
		return this.playerList;
	}
	public String getName() {
		return this.name;
	}

	public void setBudget(int b) {
		this.budget = b;
	}

	public int getBudget() {
		return this.budget;
	}

}