#------------------------
#Dana Ballou	

#------------------------

JC = javac
JFLAGS= -g

.SUFFIXES: .java .class

.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	Room.java \
	Trailer.java \
	CastingOffice.java \
	SoundStage.java \
	Player.java \
	Role.java \
	Scene.java \
	IterableNodeList.java \
	Deadwood.java
	

default: classes	

classes: $(CLASSES:.java=.class)

clean:
	rm *.class
