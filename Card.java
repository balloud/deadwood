import java.util.*;
import java.io.*;
import java.lang.*;
public class Card {

	public String name;
	public int number;
	public String description;
	public List<Part> parts = new ArrayList<Part>();
	public int budget;

	public Card() {
		this.name = "";
	}
}
