/*
* Dana Ballou
*   
*   CS345
*   Deadwood Design  
*
*/

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.lang.*;
import java.util.*;
import java.io.File;  

public class Deadwood {

    public static ArrayList<Room> roomList;
    public static ArrayList<Scene> sceneList;


    /* Main sets up for parsing, and calls playGame. Prints final winner.
    Parsing technique from Conner Anderson's post on CANVAS.  */
    public static void main(String[] args) throws IOException{

        int numPlayers = Integer.parseInt(args[0]);
        ArrayList<Set> setList = new ArrayList<Set>();
        ArrayList<Card> cardList = new ArrayList<Card>();
        Document doc = null;
        Document docBoard = null;

        try {
            File xmlFile = new File("cards.xml");
            File xmlFileBoard = new File("board.xml");
            /* card */
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(xmlFile);
            /* board */
            DocumentBuilderFactory dbFactoryBoard = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilderBoard = dbFactory.newDocumentBuilder();
            docBoard = dBuilderBoard.parse(xmlFileBoard);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(doc != null) {
            Node node = doc.getFirstChild();            
            IterableNodeList<Node> myNodes = new IterableNodeList<Node>(node.getChildNodes());
            
            for(Node n : myNodes) {
                if(n.getNodeType() == Node.ELEMENT_NODE) {
                    Element tagElement = (Element) n;
                    if(tagElement.getTagName().equals("card")) {
                        Card newCard = new Card();
                        displayCardInfo(tagElement, newCard);
                        cardList.add(newCard);
                    }
                }
            }
        }
        if(docBoard != null) {
            Node nodeBoard = docBoard.getFirstChild();
            IterableNodeList<Node> myNodesBoard = new IterableNodeList<Node>(nodeBoard.getChildNodes());

            for(Node n : myNodesBoard) {
                if(n.getNodeType() == Node.ELEMENT_NODE) {
                    Element tagElementBoard = (Element) n;                    
                    if(tagElementBoard.getTagName().equals("set")) {
                        Set newSet = new Set(); /* Make new Set to build */
                        displayBoardInfo(tagElementBoard, newSet);
                        setList.add(newSet); //Add set to list
                    }
                    if(tagElementBoard.getTagName().equals("trailer")) {
                        Set newSet = new Set(); /* Make new Set to build */
                        displayTrailerInfo(tagElementBoard, newSet);
                        setList.add(newSet); //Add set to list
                    }
                    if(tagElementBoard.getTagName().equals("office")) {
                        Set newSet = new Set(); /* Make new Set to build */
                        displayOfficeInfo(tagElementBoard, newSet);
                        setList.add(newSet); //Add set to list
                    }
                }
            }
        } 
        roomList = buildRooms(setList);   
        sceneList = buildCards(cardList);          
        Player winner = score(playGame(numPlayers));
        System.out.println("\n\n\nPlayer " + winner.getPlayerName() + " has won the game!\n\n\n");         
    }

    private static List<Player> playGame(int numPlayers) throws IOException {
        /* Init players */

        ArrayList<Player> playerArray = new ArrayList<Player>();
        boolean gameOver = false;
        boolean turnOver =false;
        boolean dayOver = true; /* Set true so first deal occurs */
        boolean noLegalMove = false;
        /* This lets me look up values for upgrading */
        Map<String, Integer> rankCredits = new HashMap<String, Integer>();
        Map<String, Integer> rankDollars = new HashMap<String, Integer>();
        rankCredits.put("2", 5);
        rankCredits.put("3", 10);
        rankCredits.put("4", 15);
        rankCredits.put("5", 20);
        rankCredits.put("6", 25); 
        rankDollars.put("2", 4);
        rankDollars.put("3", 10);
        rankDollars.put("4", 18);
        rankDollars.put("5", 28);
        rankDollars.put("6", 40);

        for (int n = 1; n <= numPlayers; n++) {
            Player newPlayer = new Player(String.valueOf(n));
            playerArray.add(newPlayer);            
        }
        /* place players in trailer */
        for (Player p : playerArray) {
            p.changeLocation(findRoom("Church"));
            p.setStatus("not acting");
            findRoom("Church").addRemovePlayer(p);
        }
        
        int daysPlayed = 0;

/****** GAME LOOP, While game not over... *************************************************************************/
        while (!gameOver) {
            
            if (daysPlayed == 4) {
                gameOver = true;
                break;
            }
            int d = 0;
            /* Deal cards after end of day */
            if (dayOver) {
                dayOver = false;
                for (Room rm : roomList) {
                    if (d >= sceneList.size()) {
                        /* Incase something goes terribly wrong . . . */
                        System.out.println("\nWe are literally out of scenes, and something is probobly broken...\n");
                        gameOver = true;
                        break;
                    }

                    if (!rm.getName().equals("office") && !rm.getName().equals("trailer")) {
                        rm.removeScene();
                        rm.addScene(sceneList.get(d));
                        sceneList.remove(d);
                        d++;
                    }
                }
            }

            for (Player activePlayer : playerArray) {

                if (gameOver == true) {
                    break;
                }
                /* Check if day is over */
                int numScenes = 0;
                for (Room roomWithScene : roomList) {
                    if (roomWithScene.doesHaveScene()) {
                        numScenes++;
                    }
                }
                if (numScenes == 1 || numScenes == 0) {  
                    daysPlayed++;                  
                    break;
                }
                /* Start of turn, read command, do action here */
                turnOver = false;

                while (!turnOver) {
                    System.out.print("\nPlease enter comand:  >");
                    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                    String s = in.readLine(); 
                    String[] inArray = s.split("\\s+");
                    /*   This switch statement is messy, I'm running out of time to divide it up though. Sorry. 
                         Bad planning on my part =(   
                         Here are all the use cases.  
                         I start each case checking if you have already made a move, 
                         in that case, you need to call 'end'.                                              */
                    switch(inArray[0]) {

                        case "who":
                            System.out.println("\nYou are: Player" +activePlayer.getPlayerName());
                            System.out.println("\nYou only have " + activePlayer.getDollars() + " dollars and " + activePlayer.getCredits() + " credits to your name...\n");
                            if (activePlayer.getStatus().equals("acting")) {
                                System.out.println("You are working on: " + activePlayer.getCurrentRole().getName());
                            }

                            break;

                        case "where":

                            if (activePlayer.getCurrentScene() != null) {
                                System.out.println("\n" + activePlayer.getLocation().getName() + " " + activePlayer.getCurrentScene().getName());
                            } else {
                                System.out.println("\n" + activePlayer.getLocation().getName());
                            }
                            System.out.println("\nLooks like you can move to these rooms: \n");
                            for (String adjRm : activePlayer.getLocation().getRoomList()) {
                                System.out.println("\t> " + adjRm);
                            }
                            System.out.println("\nThese roles are available:\n");
                            for (Role ofR : activePlayer.getLocation().getRoles()) {
                                System.out.println("\t> " + ofR.getName());
                            }
                            for (Role onR : activePlayer.getLocation().getScene().getRoleList()) {
                                System.out.println("\t> " + onR.getName());
                            }
                            break;

                        case "move":

                            if (noLegalMove) {
                                System.out.println("\nNo legal move, pal. End your turn...\n");
                                break;
                            }
                            String roomName;
                            if (inArray.length == 3) {
                                roomName = inArray[1] + " " + inArray[2];
                            } else {
                                roomName = inArray[1];
                            } 
                            Room moveRoom = findRoom(roomName);
                            /* Check if room is adjacent */
                            if (!activePlayer.getLocation().getRoomList().contains(moveRoom.getName())) {
                                System.out.println("\nThat room is not adjacent. Nice try though.\n");
                                break;
                            }
                            if (activePlayer.getStatus().equals("acting")) {
                                System.out.println("\nNo leaving in the middle of a set, son...\n");
                                break;
                            }
                            moveRoom.addRemovePlayer(activePlayer);
                            activePlayer.changeLocation(moveRoom);
                            System.out.println("\nMoved to " + moveRoom.getName() + "\n");
                            //turnOver = true;
                            noLegalMove = true;
                            break;

                        case "work":

                            if (noLegalMove) {
                                System.out.println("\nNo legal move, pal. End your turn...\n");
                                break;
                            }

                            if (inArray.length == 1) {
                                System.out.println("\nYou need to name a part to take...\n");
                                break;
                            }
                            if (activePlayer.getStatus().equals("acting")) {
                                System.out.println("\nBuddy, you can't start another job until you finish the one you got...\n");
                                break;
                            }
                            if (activePlayer.getLocation().getName().equals("office") || activePlayer.getLocation().getName().equals("trailer")) {
                                System.out.println("\nYou can't act in here. Go to a SoundStage if you are gonna be like that...\n");
                                break;
                            }

                            boolean foundRole = false;
                            /* This puts together the room name */
                            StringBuilder partNameB = new StringBuilder();
                            for (String str : inArray) {
                                partNameB.append(" ");
                                partNameB.append(str);
                            }
                            partNameB.delete(0, 6);
                            String partName = partNameB.toString();
                            /* On card role */

                            for (Role r : activePlayer.getLocation().getRoles()) {

                                if (r.getName().equals(partName)) {
                                    if (r.isTaken()) {
                                        System.out.println("Buzz off kid, someone's already here...\n");
                                        break;
                                    }
                                    if (r.getRank() > activePlayer.getRank()) {
                                        System.out.println("\nKid, you've got a ways to go before you can tackle this...\n");
                                        System.out.println("\nGo get your rank up, then come see me...\n");
                                        break;
                                    }
                                    activePlayer.setStatus("acting");
                                    activePlayer.setRole(r);
                                    r.takeRole();
                                    System.out.println("\nYipee, you are now working the (off-card) part " + partName + "\n");
                                    foundRole = true;
                                    //turnOver = true;
                                    noLegalMove = true;
                                    break;
                                } 
                            }

                            /* Off card roles------------------------------------------------------------------------------------------- */
                            
                            for (Role onCard : activePlayer.getLocation().getScene().getRoleList()) {

                                if (onCard.getName().equals(partName)) {
                                    
                                    if (onCard.isTaken()) {
                                        System.out.println("Buzz off kid, someone's already here...\n");
                                        break;
                                    }
                                    if (onCard.getRank() > activePlayer.getRank()) {
                                        System.out.println("\nKid, you've got a ways to go before you can tackle this...\n");
                                        System.out.println("\nGo get your rank up, then come see me...\n");
                                        break;
                                    }
                                    activePlayer.setStatus("acting");
                                    activePlayer.setRole(onCard);
                                    System.out.println("\nYipee, you are now working the (on-card) part " + partName + "\n");
                                    foundRole = true;
                                    //turnOver = true;
                                    noLegalMove = true;
                                    break;
                                } 
                            }

                            break;

                        case "upgrade":

                            if (noLegalMove) {
                                System.out.println("\nNo legal move, pal. End your turn...\n");
                                break;
                            }
                            int reqRank = Integer.parseInt(inArray[2]);
                            if (!activePlayer.getLocation().getName().equals("office")) {
                                System.out.println("\nUmm, you can't upgrade here. Go to the Casting Office...\n");
                                break;
                            }
                            if (reqRank < 2 || reqRank > 6) {
                                System.out.println("\nPlease enter a rank from 2 - 6. Thanks...\n");
                                break;
                            }
                            if (reqRank <= activePlayer.getRank()) {
                                System.out.println("\nNow why would you want to downgrade your rank?\n");
                                break;
                            }
                            if (inArray[1].equals("$")) {
                                int price = rankDollars.get(String.valueOf(reqRank));
                                if (activePlayer.getDollars() >= price) {
                                    activePlayer.changeRank(reqRank);
                                    activePlayer.subtractDollars(price);
                                    System.out.println("\nUpgraded to rank " + activePlayer.getRank() + "\n");
                                    //turnOver = true;
                                    noLegalMove = true;
                                    break;
                                } else {
                                    System.out.println("\nYou don't have enough money to upgrade. Come back when you do...\n");
                                    break;
                                }
                            } else if (inArray[1].equals("cr")) {
                                int credPrice = rankCredits.get(String.valueOf(reqRank));
                                if (activePlayer.getCredits() >= credPrice) {
                                    activePlayer.changeRank(reqRank);
                                    activePlayer.subtractCredits(credPrice);
                                    System.out.println("\nUpgraded to rank " + activePlayer.getRank() + "\n");
                                    //turnOver = true;
                                    noLegalMove = true;
                                    break;
                                } else {
                                    System.out.println("\nYou don't have enough credits to upgrade. Come back when you do...\n");
                                    break;
                                }
                            } else {
                                System.out.println("\nYou can't upgrade that way...\n");
                                break;
                            }

                            

                        case "reherse":

                            if (noLegalMove) {
                                System.out.println("\nNo legal move, pal. End your turn...\n");
                                break;
                            }

                            if (activePlayer.getStatus().equals("acting")) {
                                activePlayer.reherse();
                                System.out.println("\nYou've accumulated +1 rehersal chips to your little stash...\n");
                                System.out.println("You now have a Grand Total of ..... " + activePlayer.getRehersalChips() + " chips...\n");
                                //turnOver = true;
                                noLegalMove = true;
                            } else {
                                System.out.println("\nWell now why would you reherse if you aren't acting...?\n");
                                break;
                            }
                            break;

                        case "act":

                            Role currentRole = activePlayer.getCurrentRole();
                            if (noLegalMove) {
                                System.out.println("\nNo legal move, pal. End your turn...\n");
                                break;
                            }
                            /* Check if acting is an option */
                            Room room = activePlayer.getLocation();

                            if (room.getName().equals("office") || room.getName().equals("trailer")) {
                                System.out.println("\nYou can't act in here. Go to a SoundStage if you are gonna be like that...\n");
                                break;
                            }
                            if (!activePlayer.getStatus().equals("acting")) {
                                System.out.println("\nLook kid, you gotta take a role before you can act. Now bounce...\n");
                                break;
                            }
                            int dieRoll = rollDice();
                            int adjustedRoll = dieRoll + activePlayer.getRehersalChips();
                            int budget = room.getScene().getBudget();
                            System.out.println("\nYou rolled a " + dieRoll + " , and you have " + activePlayer.getRehersalChips() + " rehersal chips\n");
                            System.out.println("The move budget is "+ room.getScene().getBudget() + " million dollars.");

                            if (adjustedRoll >= budget) {
                                /*success, check role type to determine awards*/

                                if (activePlayer.getCurrentRole().getType().equals("on card")) {
                                    activePlayer.addCredits(2);
                                }
                                if (activePlayer.getCurrentRole().getType().equals("off card")) {
                                    activePlayer.addCredits(1);
                                    activePlayer.addDollars(1);
                                }
                                room.shotComplete();                                
                                noLegalMove = true;
                                System.out.println("\nSuccess? I guess you got lucky this time...\n");

                            } else {
                                /* failure */
                                if (activePlayer.getCurrentRole().getType().equals("off card")) {                                    
                                    activePlayer.addDollars(1);
                                }
                                noLegalMove = true;
                                System.out.println("\nYou failed. Pick up the pace, kid...\n");
                            }
                            /* Check if scene is a wrap, and if there is an on-card player */
                            ArrayList<Player> ocpList = new ArrayList<Player>();
                            /* Scene is wrapped */
                            if (room.getShotCounter() == 0) {
                                boolean hasOnCard = false;
                                for (Player check : room.getPlayerList()) {
                                    if (check.getStatus().equals("acting")) {                                        
                                        if (check.getCurrentRole().getType().equals("on card")) {
                                            ocpList.add(check);
                                            hasOnCard = true;
                                        }
                                    }
                                }                                
                                /* There is an on-card player */
                                System.out.println("\nLooks like you've managed to complete a scene. Here's a small reward...\n");
                                int listSize = ocpList.size();
                                if (hasOnCard == true) {                                       
                                /* Player was acting, was he on or off card? */                                
                                    List<Integer> dieRolls = new LinkedList<Integer>();
                                    /* Roll # of dice equal to budget */
                                    for (int r = 0; r < budget; r++) {
                                        dieRolls.add(rollDice());
                                    }
                                    int div = 0;
                                    /* Sorting payout by roles might not be working 100% */
                                    for (int dr : dieRolls) {
                                        ocpList.get(div % listSize).addDollars(dr);
                                        div++;
                                    }

                                for (Player ocp : room.getPlayerList()) {
                                    ocp.clearRehersalChips();
                                    if (ocp.getCurrentRole().getType().equals("off card")) {
                                        ocp.addDollars(ocp.getCurrentRole().getRank());

                                    }
                                }
                                        
                                    
                                }
                                /* Remove Role from Room/scene */
                                for (Role remRole : activePlayer.getLocation().getScene().getRoleList()) {
                                    if (remRole == currentRole) {
                                        activePlayer.getLocation().getScene().removeRole(currentRole);
                                    }
                                }
                                for (Role remRole2 : room.getRoles()) {
                                    if (remRole2 == currentRole) {
                                        room.removeRole(currentRole);
                                    }
                                }  
                                room.restoreShotCounter();
                                /* Set all player in room to 'not acting' */
                                for (Player setStat : room.getPlayerList()) {
                                    setStat.setStatus("not acting");
                                }
                            }   
                            break;

                        case "end":

                            System.out.println("\nYour turn is over. NEXT!\n");                            
                            turnOver = true;
                            noLegalMove = false;
                            break;

                        default:
                            System.out.println("\nSomething be done not how like it do. Oops, try again, that was some bad input...\n");
                    }   
                }
            }
        }
        /* Returns array of player, so we can check them all and determine winner */
        return playerArray;
    }
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /*-------------------------------------CONTRUCTION BELOW, GAME LOGIC ABOVE -----------------------------------*/
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /*This is all for parsing different levels of the xml files. It is messy. */

    private static void displayCardInfo(Element cardElement, Card newCard) {
        //System.out.println("Card Name: " + cardElement.getAttribute("name"));
        newCard.name = cardElement.getAttribute("name");
        newCard.budget = Integer.parseInt(cardElement.getAttribute("budget"));
        IterableNodeList<Node> cardData = new IterableNodeList<Node>(cardElement.getChildNodes());
        for(Node node : cardData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "scene":                        
                        newCard.number = Integer.parseInt(dataElement.getAttribute("number"));
                        newCard.description = dataElement.getTextContent();                 
                        break;
                    case "part":                        
                        Element line = (Element) node;
                        Part newCardPart = new Part();
                        newCardPart.level = Integer.parseInt(dataElement.getAttribute("level"));  
                        newCardPart.name = dataElement.getAttribute("name");                    
                        displayLineText(line, newCard, newCardPart);
                        newCard.parts.add(newCardPart);
                        break;
                    default: 
                }
            }
        }
        
    }      

    private static void displayLineText(Element tElement, Card newCard, Part newCardPart) {
        
        IterableNodeList<Node> tData = new IterableNodeList<Node>(tElement.getChildNodes());
        for(Node node : tData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "line":                        
                        newCardPart.line =  dataElement.getTextContent();
                        break;                   
                    default: 
                }
            }
        }        
    }

/*~~~~~~~~~~~~~~~~~~~~~~~Read Board Info~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    private static void displayBoardInfo(Element boardElement, Set newSet) {
        newSet.name = (boardElement.getAttribute("name"));        
        IterableNodeList<Node> boardData = new IterableNodeList<Node>(boardElement.getChildNodes());
        for(Node node : boardData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "neighbors":
                        Element neigh = (Element) node;
                        displayLineInfo(neigh, newSet);                        
                        break;
                    case "area":                        
                        Element areaEle = (Element) node;                        
                        break;
                    case "takes":                        
                        Element takeEle = (Element) node;
                        displayTakesInfo(takeEle, newSet);
                        break;
                    case "parts":                        
                        Element partEle = (Element) node;
                        displayPartsInfo(partEle, newSet);
                        
                    default: 
                }                
            }
        }        
    }

    private static void displayTrailerInfo(Element nElement, Set newSet) {
        newSet.name = "trailer";        
        IterableNodeList<Node> nData = new IterableNodeList<Node>(nElement.getChildNodes());
        for(Node node : nData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "neighbors":
                        Element neigh = (Element) node;
                        displayLineInfo(neigh, newSet);                
                        break;      
                    default: //System.out.println("Error <Missing tag info>");
                }
            }
        }        
    }

    private static void displayOfficeInfo(Element nElement, Set newSet) {
        newSet.name = "office";
        IterableNodeList<Node> nData = new IterableNodeList<Node>(nElement.getChildNodes());
        for(Node node : nData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "neighbors":
                        Element neigh = (Element) node;
                        displayLineInfo(neigh, newSet);                
                        break;      
                    default: 
                }
            }
        }
    }

    private static void displayLineInfo(Element nElement, Set newSet) {
        
        IterableNodeList<Node> nData = new IterableNodeList<Node>(nElement.getChildNodes());
        for(Node node : nData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "neighbor":
                    newSet.neighs.add(dataElement.getAttribute("name"));                                                
                        break;                   
                    default:
                }
            }
        }        
    }

    private static void displayTakesInfo(Element tElement, Set newSet) {
        //System.out.println("Takes ");
        IterableNodeList<Node> tData = new IterableNodeList<Node>(tElement.getChildNodes());
        for(Node node : tData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "take":
                    newSet.takes++;                                               
                        break;                   
                    default:
                }
            }
        }
    }

    private static void displayPartsInfo(Element pElement, Set newSet) {        
        IterableNodeList<Node> pData = new IterableNodeList<Node>(pElement.getChildNodes());
        for(Node node : pData) {
            Part newPart = new Part();
            
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "part":                        
                        Element partEle = (Element) node;
                        newPart.name = dataElement.getAttribute("name");
                        newPart.level = Integer.parseInt(dataElement.getAttribute("level"));
                        displayPartLine(partEle, newPart);
                        newSet.parts.add(newPart);
                        break;                   
                    default: 
                }
            }
        }        
    }

    private static void displayPartLine(Element pElement, Part newPart) {        
        IterableNodeList<Node> pData = new IterableNodeList<Node>(pElement.getChildNodes());
        for(Node node : pData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "line": 
                        newPart.line = dataElement.getTextContent();
                        break;
                    case "area":
                        Element partArea = (Element) node;
                        displayPartArea(partArea);                   
                    default: 
                }
            }
        }        
    }

    private static void displayPartArea(Element pElement) {    
        IterableNodeList<Node> pData = new IterableNodeList<Node>(pElement.getChildNodes());
        for(Node node : pData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "area":
                        break;                   
                    default: 
                }
            }
        }    
    }

    private static void displayAreaInfo(Element aElement) {    
        IterableNodeList<Node> aData = new IterableNodeList<Node>(aElement.getChildNodes());
        for(Node node : aData) {
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element dataElement = (Element) node;
                switch(dataElement.getTagName()) {
                    case "area":
                        break;                   
                    default: 
                }
            }
        }    
    }
  

    private static ArrayList<Room> buildRooms(ArrayList<Set> setList) {
        /* Builds Rooms
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        ArrayList<Room> rList = new ArrayList<Room>(); 
        ArrayList<Scene> sceneList = new ArrayList<Scene>();        
        for (ListIterator<Set> s = setList.listIterator(); s.hasNext(); ) { 
            Set set = s.next();
            ArrayList<String> adjLst = new ArrayList<String>();

            Room newRoom = new Room(set.name, set.neighs);
            for (String n : set.neighs) {
                adjLst.add(n);
            }
            newRoom.setShotCounter(set.takes);
            newRoom.setBackupShotCounter(set.takes);
            /* Build off-card roles */
            for (Part p : set.parts) {
                Role newRole = new Role(p.name, p.level, p.line);
                newRole.setType("off card");
                newRoom.addRole(newRole);
            }
            rList.add(newRoom);            
        }
        return rList;
    }

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*  Builds Cards (I'm calling them scenes) */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    private static ArrayList<Scene> buildCards(ArrayList<Card> cardList) {
        ArrayList<Scene> sceneList = new ArrayList<Scene>();
        for (ListIterator<Card> c = cardList.listIterator(); c.hasNext(); ) {
            Card card = c.next();
            Scene newScene = new Scene(card.name, card.description, card.number, card.budget);
            for (Part p : card.parts) {
                Role newRole = new Role(p.name, p.level, p.line);
                newRole.setType("on card");
                newScene.addRole(newRole);
            }
            sceneList.add(newScene);
        }

        return sceneList;
    }
    /* Quick helper to map string room names to Room objects */
    private static Room findRoom(String name) {
        for (Room r : roomList) {
            if (r.getName().equals(name)) {
                return r;
            } 
        }
        return null;
    }

    private static int rollDice() {

        int result = (int)(Math.random()*6) + 1;
        System.out.println("diceRoll---> " + result);
        return result;

    }
    /* Figure out finl score */
    private static Player score(List<Player> playerList) {
        Player winner = null;
        int topScore = 0;
        int totalScore = 0;
        for (Player finalPlayer : playerList) {
            totalScore = (finalPlayer.getDollars()) + (finalPlayer.getCredits()) + (5 * finalPlayer.getRank());
            if (totalScore >= topScore) {
                winner = finalPlayer;
                topScore = totalScore;
            }
        }
        return winner;
    }
}
