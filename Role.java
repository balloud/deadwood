import java.util.*;
import java.io.*;
import java.lang.*;
public class Role {

	private String name;
	private int rank;
	private String line;
	private String type;
	private boolean taken;
	

	public Role(String n, int r, String l) {
		this.name = n;
		this.rank = r;
		this.line = l;
		this.taken = false;
	}


	public String getName() {
		return this.name;
	}

	public int getRank() {
		return this.rank;
	}

	public String getLine() {
		return this.line;
	}

	public void setType(String s) {
		this.type = s;
	}

	public String getType() {
		return this.type;
	}

	public void takeRole() {
		this.taken = true;
	}
	public boolean isTaken() {
		return this.taken;
	}

	

}